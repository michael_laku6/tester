//
//  LAContext-Ext.swift
//  Tester
//
//  Created by Michael Michael on 25/04/22.
//

import Foundation
import LocalAuthentication

extension LAContext {
    func canEvaluatePolicyThrowing(policy: LAPolicy) throws {
        var error : NSError?
        canEvaluatePolicy(policy, error: &error)
        if let error = error { throw error }
    }
}
