//
//  ViewController.swift
//  Tester
//
//  Created by Michael Michael on 22/04/22.
//

import UIKit
import SnapKit
import LocalAuthentication

class ViewController: UIViewController {
    private lazy var lblFingerprintStatus: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        
        return label
    }()
    
    private lazy var btnFingerprint: UIButton = {
        let button = UIButton()
        button.backgroundColor = .blue
        button.titleLabel?.textColor = .white
        button.titleLabel?.numberOfLines = 0
        button.addTarget(self, action: #selector(onFingerprint(_:)), for: .touchUpInside)
        
        return button
    }()
    
    private var setupStatus: FingerprintHelper.SetupStatusType = .None

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUI()
        setInitial()
    }
    
    private func setUI() {
        let label: UILabel = {
            let label = UILabel()
            label.text = "Fingerprint Status"
            
            return label
        }()
        
        view.addSubview(label)
        label.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(20)
            $0.width.equalToSuperview().dividedBy(2)
            $0.centerY.equalToSuperview()
        }
        
        view.addSubview(lblFingerprintStatus)
        lblFingerprintStatus.snp.makeConstraints {
            $0.left.equalTo(label.snp.right).offset(20)
            $0.trailing.equalToSuperview().offset(-20)
            $0.centerY.equalToSuperview()
        }
        
        view.addSubview(btnFingerprint)
        btnFingerprint.snp.makeConstraints {
            $0.top.equalTo(label.snp.bottom).offset(40)
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
            $0.height.equalTo(46)
        }
    }
    
    private func setInitial() {
        FingerprintHelper.getInstance().setup { setupStatus in
            self.setupStatus = setupStatus
            
            switch setupStatus {
            case .None:
                break
            case .NotFingerprint:
                self.lblFingerprintStatus.text = "Authenticate Not Fingerprint."
                self.btnFingerprint.setTitle("Not Fingerprint", for: .normal)
                self.btnFingerprint.isEnabled = false
                
                break
            case .Success:
                self.lblFingerprintStatus.text = "Your device can authenticate TouchID."
                self.btnFingerprint.setTitle("Scan Fingerprint", for: .normal)
                self.btnFingerprint.isEnabled = true
                
                break
            case .NotEnrolled:
                self.lblFingerprintStatus.text = "Your TouchID not yet enrolled."
                self.btnFingerprint.setTitle("Enroll Fingerprint (Can't redirect to touch id settings page, only settings page)", for: .normal)
                self.btnFingerprint.isEnabled = true
                
                break
            case .Error:
                self.lblFingerprintStatus.text = "Your device cannot authenticate using TouchID."
                self.btnFingerprint.setTitle("Unable to scan", for: .normal)
                self.btnFingerprint.isEnabled = false
                
                break
            }
        }
    }
}

extension ViewController {
    @objc func onFingerprint(_ sender: UIButton) {
        if setupStatus == .NotEnrolled {
            if let url = URL(string: UIApplication.openSettingsURLString) {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            }
            
            return
        }
        
        FingerprintHelper.getInstance().run { status in
            DispatchQueue.main.async {
                switch status {
                case .Success:
                    self.lblFingerprintStatus.text = "Scan Fingerprint success"
                    
                    break
                case .AuthFailed:
                    self.lblFingerprintStatus.text = "Scan Fingerprint failed"
                    
                    break
                case .AuthCancelled:
                    self.lblFingerprintStatus.text = "Scan Fingerprint cancelled"
                    
                    break
                case .AuthPassword:
                    self.lblFingerprintStatus.text = "Scan Fingerprint changed to Password"
                    
                    break
                }
            }
        }
    }
}
