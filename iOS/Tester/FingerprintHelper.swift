//
//  FingerprintHelper.swift
//  Tester
//
//  Created by Michael Michael on 25/04/22.
//

import Foundation
import LocalAuthentication

class FingerprintHelper: NSObject {
    enum SetupStatusType {
        case None
        case Success
        case NotFingerprint
        case NotEnrolled
        case Error
    }
    
    enum StatusType {
        case Success
        case AuthFailed
        case AuthCancelled
        case AuthPassword
    }
    
    typealias SetupAction = (_ setupStatus: SetupStatusType) -> Void
    typealias Action = (_ status: StatusType) -> Void
    
    private static var shared = FingerprintHelper()
    
    static func getInstance() -> FingerprintHelper {
        return shared
    }
    
    private var setupAction: SetupAction?
    private var action: Action?
    private var context: LAContext?
}

extension FingerprintHelper {
    func setup(completion: @escaping SetupAction) {
        self.setupAction = completion
        
        self.context = LAContext()
        
        do {
            if let context = self.context {
                try context.canEvaluatePolicyThrowing(policy: .deviceOwnerAuthenticationWithBiometrics)
                
                let biometryType = context.biometryType
                
                if let setupAction = self.setupAction {
                    if biometryType == .touchID {
                        setupAction(.Success)
                    } else {
                        setupAction(.NotFingerprint)
                    }
                }
            }
        } catch let error as NSError {
            print("Cannot evaluate policy, error: \(error)")
            
            switch error.code {
            case LAError.biometryNotEnrolled.rawValue:
                if let setupAction = self.setupAction {
                    setupAction(.NotEnrolled)
                }
                
                break
            case LAError.biometryNotAvailable.rawValue:
                if let setupAction = self.setupAction {
                    setupAction(.Error)
                }
                
                break
            default:
                if let setupAction = self.setupAction {
                    setupAction(.None)
                }
                
                break
            }
        }
    }
    
    func run(completion: @escaping Action) {
        guard let context = self.context else { return }
        
        self.action = completion
        
        context.touchIDAuthenticationAllowableReuseDuration = 60
        
        context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: "Authenticate to open the app") { (success, error) in
            if success {
                if let action = self.action {
                    action(.Success)
                }
                
                return
            }
            
            if let error = error {
                switch (error as NSError).code {
                case LAError.authenticationFailed.rawValue:
                    if let action = self.action {
                        action(.AuthFailed)
                    }
                    
                    break
                case LAError.userCancel.rawValue:
                    if let action = self.action {
                        action(.AuthCancelled)
                    }
                    
                    break
                case LAError.userFallback.rawValue:
                    if let action = self.action {
                        action(.AuthPassword)
                    }
                    
                    break
                default:
                    break
                }
            }
        }
    }
}
