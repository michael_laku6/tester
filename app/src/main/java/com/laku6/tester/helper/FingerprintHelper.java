package com.laku6.tester.helper;

import android.content.Intent;
import android.os.Build;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;

import java.util.concurrent.Executor;

public class FingerprintHelper {
    public interface FingerprintCallback {
        void onNoHardware();

        void onSucceeded(final boolean enrolled);
    }

    public interface FingerprintPromptCallback {
        void onFailed();

        void onError();

        void onSucceeded();
    }

    private static FingerprintHelper fingerprintHelper;

    private AppCompatActivity activity;
    private FingerprintCallback fingerprintCallback;
    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;

    public static FingerprintHelper getInstance(final AppCompatActivity activity) {
        fingerprintHelper = new FingerprintHelper(activity);
        return fingerprintHelper;
    }

    public static FingerprintHelper getInstance() {
        return fingerprintHelper;
    }

    public FingerprintHelper(final AppCompatActivity activity) {
        this.activity = activity;
    }

    public void setup(final FingerprintCallback fingerprintCallback) {
        this.fingerprintCallback = fingerprintCallback;

        final BiometricManager biometricManager = BiometricManager.from(activity);
        switch (biometricManager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG)) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                if (fingerprintCallback != null) {
                    fingerprintCallback.onSucceeded(true);
                }

                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                if (fingerprintCallback != null) {
                    fingerprintCallback.onNoHardware();
                }

                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE: {
                if (fingerprintCallback != null) {
                    fingerprintCallback.onNoHardware();
                }

                break;
            }
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED: {
                if (fingerprintCallback != null) {
                    fingerprintCallback.onSucceeded(false);
                }

                break;
            }
        }
    }

    public void enroll(final int requestCode) {
        // Prompts the user to create credentials that your app accepts.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
            final Intent intent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
            activity.startActivityForResult(intent, requestCode);
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            final Intent intent = new Intent(Settings.ACTION_FINGERPRINT_ENROLL);
            activity.startActivityForResult(intent, requestCode);
        } else {
            final Intent intent = new Intent(Settings.ACTION_BIOMETRIC_ENROLL);
            intent.putExtra(Settings.EXTRA_BIOMETRIC_AUTHENTICATORS_ALLOWED,
                    BiometricManager.Authenticators.BIOMETRIC_STRONG);
            activity.startActivityForResult(intent, requestCode);
        }
    }

    public void prompt(final FingerprintPromptCallback fingerprintPromptCallback) {
        executor = ContextCompat.getMainExecutor(activity);
        biometricPrompt = new BiometricPrompt(activity, executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);

                if (fingerprintPromptCallback != null) {
                    fingerprintPromptCallback.onError();
                }
            }

            @Override
            public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);

                if (fingerprintPromptCallback != null) {
                    fingerprintPromptCallback.onSucceeded();
                }
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();

                if (fingerprintPromptCallback != null) {
                    fingerprintPromptCallback.onFailed();
                }
            }
        });

        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Biometric login for my app")
                .setSubtitle("Log in using your biometric credential")
                .setNegativeButtonText("Cancel")
                .build();
        biometricPrompt.authenticate(promptInfo);
    }

    public void closePrompt() {
        if (biometricPrompt != null) {
            biometricPrompt.cancelAuthentication();
        }
    }
}
