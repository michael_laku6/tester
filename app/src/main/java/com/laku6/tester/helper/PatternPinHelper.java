package com.laku6.tester.helper;

import android.app.KeyguardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;

import androidx.annotation.RequiresApi;

public class PatternPinHelper {
    public static boolean isDeviceSecured(final Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return isDeviceLocked(context);
        } else {
            return isPatternSet(context) || isPassOrPinSet(context);
        }
    }

    private static boolean isPatternSet(final Context context) {
        final ContentResolver contentResolver = context.getContentResolver();
        try {
            int lockPatternEnable = Settings.Secure.getInt(contentResolver, Settings.Secure.LOCK_PATTERN_ENABLED);
            return lockPatternEnable == 1;
        } catch (Settings.SettingNotFoundException e) {
            return false;
        }
    }

    private static boolean isPassOrPinSet(final Context context) {
        final KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE); //api 16+
        return keyguardManager.isKeyguardSecure();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private static boolean isDeviceLocked(final Context context) {
        final KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE); //api 23+
        return keyguardManager.isDeviceSecure();
    }
}
