package com.laku6.tester.helper;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AccountHelper {
    private static AccountHelper accountHelper;

    private FragmentActivity activity;

    public static AccountHelper getInstance(final FragmentActivity activity) {
        accountHelper = new AccountHelper(activity);
        return accountHelper;
    }

    public static AccountHelper getInstance() {
        return accountHelper;
    }

    public AccountHelper(final FragmentActivity activity) {
        this.activity = activity;
    }

    public boolean isPermitted() {
         return ContextCompat.checkSelfPermission(activity,
                android.Manifest.permission.GET_ACCOUNTS)
                == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission(final int requestPermissionCode) {
        ActivityCompat.requestPermissions(activity,
                new String[]{android.Manifest.permission.GET_ACCOUNTS},
                requestPermissionCode);
    }

    public List<Map<String, String>> getAccounts() {
        final AccountManager manager = (AccountManager) activity.getSystemService(Context.ACCOUNT_SERVICE);
//        final AccountManager manager = AccountManager.get(activity.getApplicationContext());
        final Account[] accounts = manager.getAccounts();
//        final Account[] accounts = manager.getAccountsByType("com.google");

        final List<Map<String, String>> values = new ArrayList<>();

        for (Account account : accounts) {
            if (!account.name.isEmpty()) {
                final Map<String, String> map = new HashMap<>();
                String email = account.name;
                String[] parts = email.split("@");
                if (parts.length > 0 && parts[0] != null) {
                    map.put(email, parts[0]);
                } else {
                    map.put(email, "");
                }

                values.add(map);
            }
        }

        return values;
    }

    public void pick(final int requestCode) {
//        final Intent intent = AccountManager.newChooseAccountIntent(null, null, new String[] { "com.google" }, true, null, null, null, null);
        final Intent intent = AccountManager.newChooseAccountIntent(null, null, null, true, null, null, null, null);
        activity.startActivityForResult(intent, requestCode);
    }
}
