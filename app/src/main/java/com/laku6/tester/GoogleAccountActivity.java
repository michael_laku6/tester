package com.laku6.tester;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.laku6.tester.helper.AccountHelper;

import java.util.List;
import java.util.Map;

public class GoogleAccountActivity extends AppCompatActivity {
    private static final int GET_ACCOUNT_REQUEST_PERMISSION_CODE = 1;
    private static final int PICK_ACCOUNT_REQUEST_CODE = 1;

    private TextView lbl_status_google_account;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_google_account);

        lbl_status_google_account = findViewById(R.id.lbl_status_google_account);

        findViewById(R.id.btn_pick).setOnClickListener(view -> AccountHelper.getInstance().pick(1));

        setStatus();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_ACCOUNT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                setStatus();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case GET_ACCOUNT_REQUEST_PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setStatus();
                }
            }
        }
    }

    private void setStatus() {
        if (AccountHelper.getInstance(this).isPermitted()) {
            final List<Map<String, String>> list = AccountHelper.getInstance().getAccounts();
            if (list.size() <= 0) {
                lbl_status_google_account.setText("Can't detect google account");
            } else {
                StringBuilder values = new StringBuilder();
                for (final Map<String, String> map : list) {
                    for (String key : map.keySet()) {
                        if (values.length() > 0) {
                            values.append(", ");
                        }
                        values.append(key);
                    }
                }

                lbl_status_google_account.setText(values);
            }
        } else {
            AccountHelper.getInstance().requestPermission(GET_ACCOUNT_REQUEST_PERMISSION_CODE);
        }
    }
}