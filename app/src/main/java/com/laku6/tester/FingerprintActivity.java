package com.laku6.tester;

import android.app.admin.DevicePolicyManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.laku6.tester.helper.FingerprintHelper;
import com.laku6.tester.helper.PatternPinHelper;

public class FingerprintActivity extends AppCompatActivity {
    private static final int FINGERPRINT_REQUEST_CODE = 1;

    private TextView lbl_status_fingerprint;
    private TextView lbl_status_security;
    private Button btn_fingerprint;
    private Button btn_security;

    private boolean hasFingerprint = false;
    private boolean fingerprintEnrolled = false;
    private boolean isDeviceSecured = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fingerprint);

        lbl_status_fingerprint = findViewById(R.id.lbl_status_fingerprint);
        lbl_status_security = findViewById(R.id.lbl_status_security);
        btn_fingerprint = findViewById(R.id.btn_fingerprint);
        btn_security = findViewById(R.id.btn_security);

        btn_fingerprint.setOnClickListener(view -> {
            if (hasFingerprint) {
                if (fingerprintEnrolled) {
                    FingerprintHelper.getInstance().prompt(new FingerprintHelper.FingerprintPromptCallback() {
                        @Override
                        public void onFailed() {
                            runOnUiThread(() -> {
                                Toast.makeText(FingerprintActivity.this, "Not recognize", Toast.LENGTH_SHORT).show();
                            });
                        }

                        @Override
                        public void onError() {
                            runOnUiThread(() -> {
                                Toast.makeText(FingerprintActivity.this, "Error", Toast.LENGTH_SHORT).show();
                            });
                        }

                        @Override
                        public void onSucceeded() {
                            runOnUiThread(() -> {
                                Toast.makeText(FingerprintActivity.this, "Your fingerprint recognized", Toast.LENGTH_SHORT).show();
                            });
                        }
                    });
                } else {
                    FingerprintHelper.getInstance().enroll(FINGERPRINT_REQUEST_CODE);
                }
            }
        });

        btn_security.setOnClickListener(view -> {
            final Intent intent = new Intent(DevicePolicyManager.ACTION_SET_NEW_PASSWORD);
            startActivity(intent);
        });

        setStatusFingerprint();
    }

    @Override
    protected void onResume() {
        super.onResume();

        setStatusSecurity();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FINGERPRINT_REQUEST_CODE) {
            setStatusFingerprint();
        }
    }

    private void setStatusFingerprint() {
        FingerprintHelper.getInstance(this).setup(new FingerprintHelper.FingerprintCallback() {
            @Override
            public void onNoHardware() {
                runOnUiThread(() -> {
                    hasFingerprint = false;
                    fingerprintEnrolled = false;

                    lbl_status_fingerprint.setText("No Hardware detected");
                    btn_fingerprint.setText("Not able to action");
                });
            }

            @Override
            public void onSucceeded(boolean enrolled) {
                runOnUiThread(() -> {
                    hasFingerprint = true;
                    fingerprintEnrolled = enrolled;

                    lbl_status_fingerprint.setText(enrolled ? "Registered" : "Not Registered");
                    btn_fingerprint.setText(enrolled ? "Test finger" : "Call Register");
                });
            }
        });
    }

    private void setStatusSecurity() {
        final boolean lastDeviceSecured = isDeviceSecured;

        isDeviceSecured = PatternPinHelper.isDeviceSecured(this);

        lbl_status_security.setText(isDeviceSecured ? "Secured" : "Not Secured");
        btn_security.setText(isDeviceSecured ? "Remove Security" : "Add Security");

        if (lastDeviceSecured && !isDeviceSecured) {
            setStatusFingerprint();
        }
    }
}
